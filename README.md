# Batch Action Sociale - Dendreo

Dendreo est une plateforme de formation utilisée par l'Action Sociale. Le but de se batch est de récupérer une liste de participants sur l'API Dendreo et de générer un fichier CSV correspondant.

## Utilisation

Pour utiliser le binaire généré, il faut lui fournir le chemin vers un fichier de configuration en format YAML : 
```shell
$ as-batch-dendreo settings.yml
```

### Configuration

Ce fichier de configuration doit contenir les informations suivantes : 
```
token: 'wavbqp07WXdC54q30pc1'                              # Token d'autentification pour l'API Dendreo
dendreo:
  url: 'https://pro.dendreo.com/association_fertiles/api'  # URL de l'API
output: 'listing.csv'                                      # Fichier de sortie
```

### Codes de retour

Code | Raison
:---:|:--------
0    | OK
1    | Pas de chemin vers la config
2    | Fichier de config introuvable ou illisible
3    | Fichier de config incomplet (détails dans le message)

const Fs = require('fs');
const Wreck = require('wreck');
const stringify = require('csv-stringify');
const YAML = require('yaml');

const { name: appName } = require('./package');

const [,, settingsPath] = process.argv;
if (!settingsPath) {
  process.stderr.write('Missing parameters. You must provide the path to the setting file.\n');
  process.stderr.write(`Usage: ${appName} settings.yml\n`);
  process.exit(1);
}

const check = (param, name) => {
  if (!param) {
    process.stderr.write(`Missing value for '${name}' in settings\n`);
    process.exit(3);
  }
};

(async () => {
  try {
    // Read and validate settings
    const settings = await Fs.promises.readFile(settingsPath, { encoding: 'utf8' });
    const { token, dendreo: { url: dendreoUrl } = {}, output } = YAML.parse(settings);
    check(token, 'token');
    check(dendreoUrl, 'dendreo.url');
    check(output, 'output');

    // Configure Dendreo client
    const date = new Date().toISOString();
    const client = Wreck.defaults({
      headers: {
        Authorization: `Token token="${token}"`,
      },
      baseUrl: dendreoUrl,
      json: 'force',
    });
    const stringifier = stringify();

    // List participants (https://developers.dendreo.com/#lister-tous-les-participants)
    const { payload } = await client.get('/participants.php');
    const outputStream = Fs.createWriteStream(output);
    let count = 0;
    const promises = payload.map(async (p) => {
      const { id_participant: participantId, id_entreprise: enterpriseId } = p;
      const [{ payload: enterprise }, { payload: inscriptions }] = await Promise.all([
        client.get(`/entreprises.php?id=${enterpriseId}`),
        client.get(`/laps.php?id_participant=${participantId}`),
      ]);
      return Promise.all(inscriptions.map(async (inscription) => {
        const { payload: formation } = await client.get(`/actions_de_formation.php?id_action_de_formation=${inscription.id_action_de_formation}&include=formateurs`);
        const { payload: s} = await client.get(`/salles_de_formation.php?id=${formation.id_salle_de_formation}`);
        const formateurs = formation.formateurs.map(f => `${f.nom} ${f.prenom}`).join('/');
        let salle;
        if (!s){
          salle = { code_postal: '', ville: '' };
        } else if(!s.code_postal){
          salle = Object.assign(s,{code_postal: ''} );
        } else{
          salle = s;
        } 

        stringifier.write([
          p.adresse, // AdrLigne1 Individu
          '', // AdrLigne2 Individu
          '', // AdrLigne3 Individu
          '', // AdrLigne4 Individu
          p.code_postal, // AdrLocalite Individu
          p.pays, // AdrPays Individu
          p.ville, // AdrVille  Individu
          '', // CategorieBeneficiaire Individu
          p.civilite, // Civilite  Individu
          '', // CommentairesIndiv Individu
          'DENDREO', // CreateurFiche Individu
          date, // DateCreation  Individu
          date, // DateMaj  Individu
          p.date_de_naissance, // DateNaissance Individu
          p.email, // EmailIndividu Individu
          'non', // NbEnfants Individu
          'non', // NbPersonnesFoyer  Individu
          p.nom, // Nom Individu
          'non', // NomPatronymique Individu
          enterprise.siret, // NumEnt  Individu
          p.prenom, // Prenom  Individu
          'non', // SituationFamille  Individu
          p.portable, // TelephoneIndividu Individu
          enterprise.raison_sociale, // NomEnt  Individu
          'non', // nbperscharge  Individu
          'non', // nbpartsfisc Individu
          '-', // branchepro  Individu
          'Non', // OptIn Individu
          date, // DateOptIn Individu
          '', // DateOptIn_lg  Individu
          'Non', // AutorisationMail  Individu
          'AS:Inscription', // Action  Evenement
          'Non', // Annulation  Evenement
          '', // CommentaireEvt  Evenement
          'DENDREO', // CreateurFiche Evenement
          date, // DateCreation  Evenement
          '', // ECO Evenement
          'Terminé', // Finalite  Evenement
          'reçu', // Flux  Evenement
          '', // Institution Evenement
          'PREPARATION RETRAITE', // MotifEco  Evenement
          'CONFERENCE', // NatureSollicitation Evenement
          1, // Niveau  Evenement
          '', // NumASEven Evenement
          '', // NumASIndividu Evenement
          '', // pieceJointesURL Evenement
          '', // PrenomTiers Evenement
          '', // RaisonSocialeTiers  Evenement
          'Interessé', // SolliciteurEvt  Evenement
          '', // IdDateLieuAS  Evenement
          '', // IdAS  Evenement
          '', // NbPers  Evenement
          '', // Thematique  Evenement
          '', // FiltreLibre Evenement
          'AS DENDREO', // filtreListe Evenement
          '', // statut  Evenement
          '', // dateConfirmation  Evenement
          '', // dateConfirmation_lg Evenement
          '', // montantCheque Evenement
          '', // numeroCheque  Evenement
          formation.intitule, // nomAS Evenement
          '', // numASDateLieu Evenement
          '', // HistoriqueInscriptionAS Evenement
          'Non', // confidentialite Evenement
          'Dendreo', // SiteGestionnaire  Evenement
          '', // commentairecourt  Evenement
          'non', // verrou  Evenement
          'non', // TRANSMIS  Evenement
          'Association Fertiles:CAP 1 - Hébergement et restauration', // Categorisation  AS
          '', // Commentaires  AS
          'MALAKOFF MEDERIC', // Copilote  AS
          'DENDREO', // CreateurFiche AS
          date, // DateCreation  AS
          '', // DateDecision  AS
          'En cours', // Etat  AS
          'DENDREO', // GestionnaireDossier AS
          '', // InterlocuteurEnt  AS
          'Institution', // ModeAction  AS
          '', // MontantAccorde  AS
          '', // MontantPaye AS
          '', // MontantPropose  AS
          formation.intitule, // NomAS AS
          enterprise.raison_sociale, // NomEnt  AS
          '', // NomContactEnt AS
          '', // NumActionSociale  AS
          '', // Objectifs AS
          'MALAKOFF MEDERIC', // Pilote  AS
          '', // Portable  AS
          formateurs, // RespCopilote  AS
          formateurs, // RespPilote  AS
          '', // ResteAFinancer  AS
          '', // ResteAPayer AS
          '', // SiretEnt  AS
          salle.intitule, // Site  AS
          '', // SiteEnt AS
          '', // SituProSta  AS
          '', // TelEnt  AS
          'Association fertiles', // Thematique  AS
          'CAP 1 - Hébergement et restauration', // Theme AS
          '', // PopulationAge AS
          '', // AnneeExercice AS
          '', // compteurDateLieu  AS
          '', // laction AS
          salle.code_postal, // CodePostal  DATE&LIEU
          formation.date_debut, // DateManif DATE&LIEU
          salle.code_postal.substr(0, 2), // Dept  DATE&LIEU
          salle.ville, // Lieu  DATE&LIEU
          'Réunion', // TypeAction  DATE&LIEU
          '', // interlocuteur DATE&LIEU
          formation.nb_participants_max, // NbPlaces  DATE&LIEU
          formation.total_participants, // NbInscrits  DATE&LIEU
          '', // commentaire DATE&LIEU
          '', // exportweb DATE&LIEU
          '', // NbAttentes  DATE&LIEU
          '', // NbPresents  DATE&LIEU
          '', // NbDesistements  DATE&LIEU
          '', // NbRenoncements  DATE&LIEU
          '', // NbReports DATE&LIEU
          0.00, // MontantPart DATE&LIEU
          '', // Adresse4  DATE&LIEU
          '', // Localite  DATE&LIEU
          '', // Ville DATE&LIEU
          '', // CP  DATE&LIEU
        ]);
        count += 1;
        console.log('wrote', count);
      }));
    });
    stringifier.pipe(outputStream)
      .on('finish', () => process.stdout.write('Batch completed successfully.\n'));
    await Promise.all(promises);
    stringifier.end();
  } catch (e) {
    process.stderr.write(`${e.message}\n`);
    process.exit(2);
  }
})();
